# http4s IO quickstart

An PoC project created to experiment on http4s and IO.

Its initial structure has been created out of [`http4s/http4s-io`](https://github.com/http4s/http4s-io.g8)
g8 template.
