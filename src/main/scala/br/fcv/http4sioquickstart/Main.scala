package br.fcv.http4sioquickstart

import cats.effect.{IO, IOApp}

object Main extends IOApp.Simple {
  def run:IO[Unit] = Http4sioquickstartServer.run
}
